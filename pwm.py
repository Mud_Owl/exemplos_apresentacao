import time
import mraa

led = mraa.Pwm(3)
led.period_us(700)
led.enable(True)
valor = 0.0

potPin = 0
pot = mraa.Aio(potPin)
potVal = 0.0

while True:
    potVal = float(pot.read())
    led.write(potVal/1023.0)
