import mraa
import time
import sys

while 1:
	#flag
	flag = 0;
	# Saida digital - LED
	ledPin = mraa.Gpio(3)
	ledPin.dir(mraa.DIR_OUT)
	ledPin.write(0)

	#Entrada digital - botao
	buttonPin = mraa.Gpio(4)
	buttonPin.dir(mraa.DIR_IN)

	#leitura do botao
	flag = buttonPin.read()
	print "Estado: " + str(flag)
	if flag == 1:
		#acende o LED 
		ledPin.write(1)
	else:
		#apaga o LED
		ledPin.write(0)