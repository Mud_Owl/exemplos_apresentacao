import pyupm_i2clcd as lcd
import mraa
import time
import sys
import math

# pino analogico - temperatura
tempPin=1
temp = mraa.Aio(tempPin)
tempVal = 0

# display - lcd
lcdDisplay = lcd.Jhd1313m1(0, 0x3E, 0x62)
while 1:
        # read pot/print/convert to string/display on lcd
    tempAux = float(temp.read())
    bVal = 4275
    resistenciaVal = (1023 - tempAux) * 10000 / tempAux
    celsiusVal = 1 / (math.log(resistenciaVal / 10000) / bVal + 1 / 298.15) - 273.15
    tempVal = celsiusVal

    print " Temp: "+ str(tempVal)   

    potStr = "Temperatura:"
    lcdDisplay.setCursor(0, 0)
    lcdDisplay.setColor(200,0,0)
    lcdDisplay.write(potStr)
        
        
    temStr = "Temp: "+str(tempVal) + " Celsius"
    lcdDisplay.setCursor(1, 0)
    lcdDisplay.write(temStr)

    time.sleep(3)  