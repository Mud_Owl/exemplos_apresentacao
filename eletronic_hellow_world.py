import mraa
import time
import sys

while 1:
	# Saida digital - LED
	ledPin = mraa.Gpio(3)
	ledPin.dir(mraa.DIR_OUT)
	ledPin.write(0)

	#acende o LED por 5 segundos
	ledPin.write(1)
	time.sleep(1)
	#apaga o LED por 5 segundos
	ledPin.write(0)
	time.sleep(1)
